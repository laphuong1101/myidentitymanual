﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using MyIdentityManual.Models;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

[assembly: OwinStartup(typeof(MyIdentityManual.App_Start.IdentityConfig))]
namespace MyIdentityManual.App_Start
{
    public class IdentityConfig
    {
        public void Configuration(IAppBuilder app)
        {
            // khoi tao callback de goi DbContext
            app.CreatePerOwinContext(() => new MyManualDbContext());
            // khoi tao callback de goi MyUserManager
            app.CreatePerOwinContext<MyUserManager>(MyUserManager.Create);
            // khoi tao callback de goi MyRoleManager
            app.CreatePerOwinContext<MyRoleManager>(MyRoleManager.Create);

            app.CreatePerOwinContext<MySignInManager>(MySignInManager.Create);
            // khoi tao cookie de luu thong tin tam thoi cua user khi dang ky va dang nhap
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                // duong dan den action login
                LoginPath = new PathString("/Home/Login"),
            });


            // khoi tao role khi app vua chay
            MyManualDbContext context = new MyManualDbContext();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            if (!roleManager.RoleExists("manager"))
            {
                // khoi tao 1 role co ten la manager
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "manager";
                roleManager.Create(role);
            }
        }

        private void createRoles()
        {
            // su dung doan code nay de tao RoleManager

        }
    }
}