﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyIdentityManual.Models
{
    public class MyManualDbContext : IdentityDbContext<MyUser>
    {
        public MyManualDbContext() : base("MyManualConnectionString")
        {

        }
        // cac DbSet cua User va Role he thong se tu dong tao
        // chi khai bao them cac DbSet khac de su dung tuong ung voi cac bang trong database
    }
}