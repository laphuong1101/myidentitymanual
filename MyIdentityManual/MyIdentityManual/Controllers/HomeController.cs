﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using MyIdentityManual.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MyIdentityManual.Controllers
{
        // phai bat buoc dang nhap moi cho truy cap vao controller
    //[Authorize]
    public class HomeController : Controller
    {
        private MyUserManager _userManager;
        private MyRoleManager _roleManager;
        private MySignInManager _signInManager;
        private IAuthenticationManager authenticationManager;

        public MyUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<MyUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public IAuthenticationManager AuthenticationManager
        {
            get
            {
                return authenticationManager ?? HttpContext.GetOwinContext().Authentication;
            }
            private set
            {
                authenticationManager = value;
            }
        }

        public MyRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().GetUserManager<MyRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        public MySignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<MySignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }


        //constructor khong the khoi tao httpcontext => vi no duoc tao ra trc request la khong the
        //public HomeController()
        //{
        //    _userManager = HttpContext.GetOwinContext().GetUserManager<MyUserManager>();
        //    _roleManager = HttpContext.GetOwinContext().GetUserManager<MyRoleManager>();
        //    authenticationManager = HttpContext.GetOwinContext().Authentication;
        //}


        public ActionResult Index()
        {
            return View();
        }

        //[Authorize(Roles = "admin")]
        [MyAuthorize(Roles = "admin")]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

            // yeu cau user phai co du cac quyen nay moi duoc truy cap vao action
        //[Authorize(Roles = "abc")]
        //[Authorize(Roles = "admin")]
           // liet ke mot trong so cac quyen co the truy cap vao action
        [Authorize(Roles = "admin,abc")]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> Register(MyUser user)
        {
            //var userManager = HttpContext.GetOwinContext().GetUserManager<MyUserManager>();
            //var roleManager = HttpContext.GetOwinContext().Get<MyRoleManager>();
            //var authentication = HttpContext.GetOwinContext().Authentication;

            var User = new MyUser()
            {
                Email = user.Email,
                UserName = user.UserName
            };

            var result = await UserManager.CreateAsync(User, user.PasswordHash);
            if (result.Succeeded)
            {
                //var ident = await UserManager.CreateIdentityAsync(User,
                //DefaultAuthenticationTypes.ApplicationCookie);

                //AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = false }, ident);

                // can phai BAT BUOC dang ky khoi tao 1 IUserTokenProvider 
                string code = await UserManager.GenerateEmailConfirmationTokenAsync(User.Id);
                // goi den action ConfirmEmail de xac thuc
                var callbackUrl = Url.Action("ConfirmEmail", "Home", new { userId = User.Id, code = code }, protocol: Request.Url.Scheme);
                // dang nhap email va click va link de xac thuc tai khoan
                await UserManager.SendEmailAsync(User.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                return RedirectToAction("Index", "Home");
            }
            else
            {
                string error = JsonConvert.SerializeObject(result.Errors);
                Debug.WriteLine(error);
                return RedirectToAction("About", "Home");
            }
        }

        // action xac thuc hien cong viec xac thuc email
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "Index" : "Error");
        }

        public async System.Threading.Tasks.Task<ActionResult> CreateRoleAsync()
        {
            var roleManager = HttpContext.GetOwinContext().Get<MyRoleManager>();
            var userManager = HttpContext.GetOwinContext().GetUserManager<MyUserManager>();
            //bat buoc phai la async Task
         //   await roleManager.CreateAsync(new MyRole() { Name = "admin" });
            await userManager.AddToRoleAsync("7b272bea-1ea8-4913-b87d-7b980af4ee03", "admin");
         //   await userManager.AddToRoleAsync("0be65492-1c5a-49d8-a08f-55e29aa1842b", "abc");

            // Xoa role
         //   await userManager.RemoveFromRoleAsync("0be65492-1c5a-49d8-a08f-55e29aa1842b", "abc");

            return Redirect("Index");
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> Login(MyUser user)
        {
            //var userManager = HttpContext.GetOwinContext().GetUserManager<MyUserManager>();
            //var roleManager = HttpContext.GetOwinContext().Get<MyRoleManager>();
            //var authentication = HttpContext.GetOwinContext().Authentication;

            var User = await UserManager.FindAsync(user.UserName, user.PasswordHash);
            
            if (User != null)
            {
                var ident = await UserManager.CreateIdentityAsync(User, DefaultAuthenticationTypes.ApplicationCookie);
                AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = false }, ident);
                Session["UserName"] = "this is " + User.UserName;
            }
            return Redirect("Index");
        }

        public ActionResult LogOff()
        {
            var authentication = HttpContext.GetOwinContext().Authentication;
            authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        public ActionResult CheckRole()
        {
            // tra ve danh sach cac role ma user dang co
            IEnumerable<string> Roles = UserManager.GetRoles("0be65492-1c5a-49d8-a08f-55e29aa1842b");
            ViewBag.Roles = Roles;

            // kiem tra xem user co quyen nay k => co thi tra ve true k thi tra ve false
            ViewBag.IsCheckRole = UserManager.IsInRole("0be65492-1c5a-49d8-a08f-55e29aa1842b", "admin");

            //tra ve ten user hien dang dang nhap
            ViewBag.Name = User.Identity.GetUserName();

            ViewBag.Check = UserManager.IsEmailConfirmed("0be65492-1c5a-49d8-a08f-55e29aa1842b");
            return View();
        }

        public ActionResult Info(ManageMessageId? message)
        {
            // message khi co mot thao tac nao do goi lai action nay
            ViewBag.Msg = message;
            //lay UserId tu cookie
            var userId = User.Identity.GetUserId();
            var user = UserManager.FindById(User.Identity.GetUserId());

            return View(user);
        }

        // action doi password
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> ChangePassword(ChangePasswordModel model)
        {
            // thuc hien doi pass cho user
            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            // neu doi pass thanh cong se dang nhap va chuyen ve trang index
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    var ident = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
                    AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = false }, ident);
                }
                return RedirectToAction("Info", new { Message = ManageMessageId.SetPasswordSuccess});
            }
            return View(model);
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }


        // action tim lai password
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordModel model)
        {
            var user = await UserManager.FindByEmailAsync(model.Email);
            // kiem tra neu user k ton tai hoac email chua xac nha tu tra ve view thong bao
            if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
            {
                // Don't reveal that the user does not exist or is not confirmed
                return View("ComfirmEmail");
            }

            string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
            var callbackUrl = Url.Action("ResetPassword", "Home", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
            await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
            return RedirectToAction("ForgotPasswordConfirmation", "Home");

        }

        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        // action reset password
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        [HttpPost]
        public async Task<ActionResult> ResetPassword(ResetPasswordModel model)
        {
            // tim kiem user theo Email sau khi xac thuc duong link trong email, va dc chuyen va trang reset password
            // thong tin cua user se duoc thu thap qua form ResetPassword
            var user = await UserManager.FindByEmailAsync(model.Email);
            // neu khong tim thay user thi redirect ve trang thong bao loi
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return View("Error");
            }

            // reset pw va tra ve gia tri bool
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            // neu result la true thi chuyen va trang thong bao thanh cong 
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Home");
            }
            return null;
        }

        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        // test SignInManager
        public ActionResult TestSignInManager()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> TestSignInManager(LoginModel model, string returnUrl)
        {
            var user = await UserManager.FindByNameAsync(model.UserName);
            var rs = await UserManager.IsEmailConfirmedAsync(user.Id);

            if (!rs)
            {
                return View("ComfirmEmail");
            }

            var result = await SignInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        public ActionResult AlertAuthorize()
        {
            return View();
        }

    }
}