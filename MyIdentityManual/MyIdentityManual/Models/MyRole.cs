﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyIdentityManual.Models
{
    public class MyRole : IdentityRole
    {
        public MyRole() : base() { }
        public MyRole(string name) : base(name) { }

        // thuoc tinh them cho role
        public string MyPropertyCustom { get; set; }

    }
}