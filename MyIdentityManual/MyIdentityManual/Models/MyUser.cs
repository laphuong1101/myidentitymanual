﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyIdentityManual.Models
{
    public class MyUser : IdentityUser
    {
        // day la thuoc tinh muon them vao cho 1 user
        public string Rollnumber { get; set; }
    }
}