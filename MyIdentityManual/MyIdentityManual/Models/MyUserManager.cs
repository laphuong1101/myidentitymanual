﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyIdentityManual.Models
{
    public class MyUserManager : UserManager<MyUser>
    {
        public MyUserManager(IUserStore<MyUser> store) : base(store)
        {
        }

        // method create an object MyUserManager
        public static MyUserManager Create(IdentityFactoryOptions<MyUserManager> options, IOwinContext context)
        {
            // khoi tao doi tuong MyUserManager
            var manager = new MyUserManager(new UserStore<MyUser>(context.Get<MyManualDbContext>()));
            manager.EmailService = new EmailService();
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                // tao ra ma code de doi pass hoac xac nhan email
                manager.UserTokenProvider =
                    new DataProtectorTokenProvider<MyUser>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }
    }
}