﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyIdentityManual.Models
{
    public class MyRoleManager : RoleManager<MyRole>
    {
        public MyRoleManager(IRoleStore<MyRole, string> store) : base(store)
        {

        }

        // method create an object MyRoleManager
        public static MyRoleManager Create(IdentityFactoryOptions<MyRoleManager> options, IOwinContext context)
        {
            var manager = new MyRoleManager(new RoleStore<MyRole>(context.Get<MyManualDbContext>()));
            return manager;
        }
    }
}